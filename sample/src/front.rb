require 'sinatra'
require 'sinatra/json'
require 'net/http'
require 'json'

set :bind, ENV['HOST']
set :port, ENV['PORT']

before do
  url = ENV['APP_URL']
  url = "http://" + url unless url =~ /^http/
  @uri = URI url

  @reply = {}
end

def create_response code, key, value
  r = { front_version: "flat" }
  r[:code] = code
  r[key] = value
  r
end

get '/' do
  begin
    Net::HTTP.start @uri.host, @uri.port do |http|
      req = Net::HTTP::Get.new @uri

      res = http.request req 
      @reply[:code], @reply[:key], @reply[:value] = 
        200, :message, JSON.parse(res.body)
    end
  rescue => err
    @reply[:code], @reply[:key], @reply[:value] = 
      500, :error, err.message
  end

  json create_response(@reply[:code], @reply[:key], @reply[:value])
end

get '/auth' do
  begin
    Net::HTTP.start @uri.host, @uri.port do |http|
      req = Net::HTTP::Get.new @uri
      req['dev-name'] = 'joao'

      res = http.request req 
      @reply[:code], @reply[:key], @reply[:value] = 
        200, :message, JSON.parse(res.body)
    end
  rescue => err
    @reply[:code], @reply[:key], @reply[:value] = 
      500, :error, err.message
  end

  json create_response(@reply[:code], @reply[:key], @reply[:value])
end
