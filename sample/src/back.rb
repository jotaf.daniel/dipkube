require 'sinatra'
require 'sinatra/json'

set :bind, ENV['HOST']
set :port, ENV['PORT']

get '/' do
  json code: 200, version: "v3"
end
