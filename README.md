# Dipkube

apply Development in Production to your dev workflow

## Conventions

Dipkube counts with some structure in your app and in your cluster:

1. When developing a new version, Dipkube will config the cluster to filter by a header in the requests: make sure your app will always look for a token in the environment variables to inject it into the headers

2. The identifier goes in the header of the request, so make sure that your app do not treat any request with a development identifier as production data


## Commands

### Help

Usage: `dipkube help`

Shows this help message


### Init

Usage: `dipkube init NAME [OPTIONS]`

Sets the cluster context to deal with Development in Production

|**argument**|description|
|:----------:|-----------|
| `NAME` |Name of the deployment to be created to host the proxy inside the cluster|


|**option**| alt |params|description|
|:--------:|:--- |:----:|-----------|
|`--virtual-service`|`-v`|`string`|Name of the Istio's Virtual Service that controls the deployment in production<br>(default: NAME-virtual-service;)|
|`--destination-rule`|`-d`|`string`|Name of the Istio's Destination Rule that defines subsets of deployments in production<br>(default: NAME-destination-rule)|
|`--clean`|`-c`|N/A|If set, this will clean the cluster context, restoring its initial state|


### Run

Usage: `dipkube run NAME CONTEXT PORT [OPTIONS]`

Runs a local development container and connects it with the proxy in the cluster


|**argument**|description|
|:----------:|-----------|
|`NAME`|Name of the deployment that has the proxy in the cluster|
|`CONTEXT`|The context for the Docker image to be built based on<br>(use `.` for current directory or the path to context)|
|`PORT`|The local port that Telepresence will be listening on|


|**option**| alt |params|description|
|:-------- |:--- |:----:|-----------|
|`--file`|`-f`|`string`|Path to the Dockerfile|
|`--image`|`-i`|`string`|Name of the Docker image to be built|
|`--container`|`-c`|`string`|Name of the Docker container to be ran|


### Sample

Usage: `dipkube sample SUBCMD`

Manages the Sample app in the k8s infrastructure

|**subcommand**|description|
|:------------:|-----------|
|`SUBCMD`| Manage the sample app in the cluster<br>(`apply` or `delete`, conveniently the same as `kubectl`)|


## Tutorial

Dipkube comes with a sample app as tutorial.

### Up

1. make sure you're in the root of the project

2. Deploy the basic app with `dipkube sample apply`

3. Set the ground to development with `dipkube init my-app`

### Down

1. Clean your work with `dipkube init my-app -c` (note: `-c` is the same as `--clean`)

2. Delete the sample app with `dipkube sample delete`