require_relative './lib/cli'
require_relative './src/help'
require_relative './src/init'
require_relative './src/run'
require_relative './src/sample'

class Dipkube < CLI
  class << self
    def read_command(argv)
      is_help = argv.none?(/(help|init|run|sample)/) || argv.any?(/help/)
      return Help, [] if is_help

      {Init => /^init$/, Run => /^run$/, Sample => /^sample$/}.each do |cmd, rgx|
        return cmd, argv.delete_if { |arg| arg =~ rgx } if argv.any? rgx
      end
    end
  end
end

Dipkube.start ARGV
