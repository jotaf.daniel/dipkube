require_relative '../lib/command'

class Help < Command
  class << self
    private

    def parse(argv); end
    def configure; end

    def do
      File.open './assets/help.txt' do |f|
        puts f.read
      end
    end
  end
end
