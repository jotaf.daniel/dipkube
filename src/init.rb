require 'yaml'

require_relative '../lib/command'

class Init < Command
  class << self
    STORE_DIR="./.dip"

    private

    def parse(argv)
      @argv = argv
      @clean = @argv.any? /^(--clean|-c)$/

      @vsvc = value_of_flag /^(-v|--virtual-service)$/
      @desr = value_of_flag /^(-d|--destination-rule)$/

      @deploy = @argv[0]
    end

    def value_of_flag(rgx)
      index = @argv.find_index  { |arg| arg =~ rgx }

      if index
        @argv.delete_at index
        @argv.delete_at index
      else
        nil
      end
    end

    def configure
      @vsvc ||= "#{@deploy}-virtual-service"
      @desr ||= "#{@deploy}-destination-rule"
    end

    def do
      clean_up if @clean

      vsvc = get_istio 'virtualservice', @vsvc
      desr = get_istio 'destinationrule', @desr

      last_version = desr['spec']['subsets'][-1]['name'][(1..-1)]
      new_version = "v#{last_version.to_i + 1}"
      desr['spec']['subsets'] << {
        "name" => new_version,
        "labels" => {
          "version" => new_version
        }
      }

      host = vsvc['spec']['http'][0]['route'][0]['destination']['host']
      token = "F1AB31D41EC1"

      condition = {
        "match" => [
          { "headers" => {
            "devel" => { "exact" => token }
          }}
        ],
        "route" => [
          { "destination" => {
            "host" => host,
            "subset" => new_version
          }}
        ]
      }

      vsvc['spec']['http'][(0..1)] = condition, vsvc['spec']['http'][0]

      Dir.mkdir '.tmp' unless Dir.exist? '.tmp'

      desr_tmp_file = '.tmp/desr.yaml'
      vsvc_tmp_file = '.tmp/vsvc.yaml'

      File.open desr_tmp_file,'w' do |f|
        f.puts desr.to_yaml
      end

      File.open vsvc_tmp_file,'w' do |f|
        f.puts vsvc.to_yaml
      end

      `kubectl apply -f #{desr_tmp_file}`
      `kubectl apply -f #{vsvc_tmp_file}`

      File.delete desr_tmp_file
      File.delete vsvc_tmp_file
    end

    def get_istio(k8s_kind, name)
      istio_obj = YAML.load `kubectl get #{k8s_kind} #{name} -o yaml`
      store istio_obj, name

      istio_obj
    end

    def store(istio_obj, filename)
      File.open "#{STORE_DIR}/#{filename}.yaml", 'w' do |f|
        f.puts istio_obj.to_yaml
      end
    end

    def clean_up
      [@vsvc, @desr].each do |filename|
        full_filename = "#{STORE_DIR}/#{filename}.yaml"
        `kubectl delete -f #{full_filename}`
        `kubectl apply -f #{full_filename}`
        File.delete full_filename
      end

      exit 0
    end
  end
end
