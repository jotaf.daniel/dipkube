require_relative '../lib/command'

class Sample < Command
  class << self
    private

    def parse(argv)
      mtc = argv.join.match(/(apply|delete)/)
      @command =
        if mtc.respond_to? :[]
          mtc[0]
        else
          nil
        end

      raise RuntimeError, "Unknown command #{argv.join " "}" if @command.nil?
    end

    def configure; end

    def do
      `kubectl #{@command} -f sample/k8s`
      `kubectl #{@command} -f sample/istio`
    end
  end
end
