require_relative '../lib/command'

class Run < Command
  class << self
    private

    def parse(argv); end
    def configure; end

    def do
      puts "Successfully called Run"
    end
  end
end
