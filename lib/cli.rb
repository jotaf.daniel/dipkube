class CLI
  class << self
    def start(argv)
      cmd, args = self.read_command argv
      call cmd, args
    end

    private

    def read_command(argv)
      raise RuntimeError, "Not implemented 'read_command'"
    end

    def call(command, argv)
      command.run argv
    end
  end
end
