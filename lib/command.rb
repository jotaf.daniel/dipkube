class Command
  class << self
    def run(argv)
      parse argv
      configure
      self.send :do
    end

    private

    %i{ parse configure do }.each do |m|
      define_method m do
        raise RuntimeError, "Not implemented '#{m}'"
      end
    end
  end
end
